
#include <stdio.h>
#include "Core.h"
#include "SdlUtil.h"

SDL_Window   *_window;
SDL_Renderer *_renderer;

void draw_rect_data(TTF_Font *font, int x, int y, SDL_Rect pos, SDL_Renderer *renderTarget);

int init() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) 
		return 1;
	if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
		return 1;
	if (TTF_Init() != 0)
		return 1;
	return 0;
}

void cleanup() {
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char *argv[]) {

	if (init() != 0)
		return 1; // failed to initialise

	_window = SDL_CreateWindow(NULL, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, 0);
	if (_window == NULL)
		return 1; // failed to create window
	
	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
	if (_renderer == NULL)
		return 1; // failed to create renderer

	// render white background colour
	SDL_SetRenderDrawColor(_renderer, 0xFF, 0xFF, 0xFF, SDL_ALPHA_OPAQUE);

	// load resources
	TTF_Font *monoFont = LoadFont("data/fonts/LiberationMono-Regular.ttf", 18);

	spritesheet_s horse;
	horse.frameCount = 11;
	horse.sprite.x = 0;
	horse.sprite.y = 0;
	horse.sprite.w = 84;
	horse.sprite.h = 68;
	horse.currentFrame = 0;
	horse.sprite.texture = LoadTexture("data/images/horse.bmp", 0xFFFF00, _renderer);

	spritesheet_s character;
	character.sprite.texture = LoadTexture("data/images/character.png", 0xFF00FF, _renderer);
	character.sprite.x = 0;
	character.sprite.y = 0;
	character.sprite.w = 32;
	character.sprite.h = 32;
	character.currentFrame = 0;
	character.frameCount = 8;

	const int FPS = 30;
	Uint32 start;

	spritesheet_s *anim = &character;
	SDL_Rect dst = { anim->sprite.x, anim->sprite.y, anim->sprite.w, anim->sprite.h };
	SDL_Rect src = { 0, 0, anim->sprite.w, anim->sprite.h };

	int animSpeed = 3;


	SDL_Event evt;
	bool run = true;
	while (run) {

		start = SDL_GetTicks();

		// event processing
		while (SDL_PollEvent(&evt)) {
			if (evt.type == SDL_QUIT) run = false;
		}

		// render
		
		anim->currentFrame++;
		if (anim->currentFrame == anim->frameCount)
			anim->currentFrame = 0;
		
		src.x = anim->currentFrame * anim->sprite.w;

		// clear
		SDL_RenderClear(_renderer);

		/** rendering here **/

		draw_rect_data(monoFont, 0, 100, src, _renderer);
		SDL_RenderCopy(_renderer, anim->sprite.texture, &src, &dst);

		// present
		SDL_RenderPresent(_renderer);

		// frame rate limiter
		if (1000 / FPS > SDL_GetTicks() - start)
			SDL_Delay(1000 / FPS - (SDL_GetTicks() - start));

	}


	SDL_DestroyTexture(horse.sprite.texture);
	SDL_DestroyTexture(character.sprite.texture);

	cleanup();

	return 0;
}

// dump SDL_Rect to screen
void draw_rect_data(TTF_Font *font, int x, int y, SDL_Rect pos, SDL_Renderer *renderTarget) {

	char text[500];
	sprintf(text, "x: %d y: %d w: %d h: %d", pos.x, pos.y, pos.w, pos.h);

	SDL_Rect dst = { x, y, 0, 0 };
	TTF_SizeText(font, text, &dst.w, &dst.h);

	SDL_Color fg = { 0, 0, 0, 0xFF }; // black
	SDL_Surface *surface = TTF_RenderText_Solid(font, text, fg);
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderTarget, surface);
	SDL_FreeSurface(surface);
	SDL_RenderCopy(renderTarget, texture, NULL, &dst);
	SDL_DestroyTexture(texture);
}


