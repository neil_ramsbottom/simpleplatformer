
#include "Core.h"
#include "SdlUtil.h"

// Load texture from image file on disk
SDL_Texture *LoadTexture(const char *filename, Uint32 colorKey, SDL_Renderer *renderTarget) {

	SDL_Surface *tempSurface = IMG_Load(filename);

	if (tempSurface == NULL) {
		// todo: log
		return NULL;
	}

	if (colorKey > 0)
		SDL_SetColorKey(tempSurface, SDL_TRUE, colorKey);

	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderTarget, tempSurface);
	SDL_FreeSurface(tempSurface);

	if (texture == NULL) {
		// todo: log
		return NULL;
	}

	return texture;
}

// Load a TrueType font of the specified point size
TTF_Font *LoadFont(const char *filename, const int size) {

	TTF_Font *font = TTF_OpenFont(filename, size);

	if (!font) {
		// TODO: log error
		return NULL;
	}

	return font;
}
