
#pragma once

#include "Core.h"

typedef struct sprite_s {
	int x;
	int y;
	int w;
	int h;
	SDL_Texture *texture;
} sprite_s;

typedef struct spritesheet_s {
	int currentFrame;
	int frameCount;
	sprite_s sprite;
} spritesheet_s;

typedef struct font_s {
	int       sz; /* size */
	TTF_Font *f;  /* font */
} font_s;

TTF_Font *LoadFont(const char *filename, const int size);
SDL_Texture *LoadTexture(const char *filename, Uint32 colorKey, SDL_Renderer *renderTarget);
