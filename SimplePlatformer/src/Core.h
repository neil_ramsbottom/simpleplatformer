

#include <sdl/SDL.h>
#include <sdl/SDL_image.h>
#include <sdl/SDL_ttf.h>

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2Main.lib")
#pragma comment(lib, "SDL2_Image.lib")
#pragma comment(lib, "SDL2_TTF.lib")
